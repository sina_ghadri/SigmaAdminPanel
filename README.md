<p align="center">
  <a href="http://projects.sinaghadri.com/SigmaAdminPanel">
    <img src="./img/sigmaSoft.jpg" alt="" width=150 height=150>
  </a>
</p>

<br>

# SigmaAdminPanel
 Angular admin panel front-end framework

## Project Structure tree

```
SigmaAdminPanel /src
├── app
|   ├──layout
|   |   ├──components
|   |   ├──models
|   |   └──services
|   |
|   ├──[subSystemTest1]
|   |   ├──components
|   |   |   ├──[firstPage]
|   |   |   |   ├──first-page.component.html
|   |   |   |   ├──first-page.component.scss
|   |   |   |   ├──first-page.component.spec.ts (optional->forTest)
|   |   |   |   └──first-page.component.ts
|   |   |   |
|   |   |   |                    
|   |   |   ├──[secondPage]
|   |   |   └──[..........]
|   |   |
|   |   ├──models
|   |   └──services
|   |
|   ├──[subSystemTest2]
|   └──[..............]
|
|
├── assest
|── fonts
└── staticsApi
     ├──admin-panel
     |   ├──admin-panel
     |       ├──loader
     |       |   ├──get.json
     |       |
     |       ├──authentication
     |   
     └──
     
     

```


## use AngularCli
 please for create any file use AngularCli(commanline)

### Subsystem Module
 for create new Subsystem Module

```
ng g m [subSystemTest] --routing

```

 after import [SubSystemTestModule] in app.module 

### Subsystem components
 for create new Subsystem components

```
ng g c [subSystemTest]/components/[firstPage]

```

 after add below code in [SubSystemTestRouting].Module 

```
  { path: '[firstPage]', component: [FirstPageComponent] } 

``` 

### Subsystem services
 for create new Subsystem services

```
ng g service  [subSystemTest]/services/[my-new-service]

```

 after add  [MyNewServiceService] to provider of  [SubSystemTestModule] 

### Subsystem models
 for create new Subsystem models

### Subsystem models class

```
ng g class  [subSystemTest]/models/[my-new-class]

```
### Subsystem models enums

```
ng g enum [subSystemTest]/models/[my-new-enum]

```
### Subsystem models interface

```
ng g interface  [subSystemTest]/models/[my-new-interface]

```


 





# Project tree

.
 * [tree-md](./tree-md)
 * [dir2](./dir2)
   * [file21.ext](./dir2/file21.ext)
   * [file22.ext](./dir2/file22.ext)
   * [file23.ext](./dir2/file23.ext)
 * [dir1](./dir1)
   * [file11.ext](./dir1/file11.ext)
   * [file12.ext](./dir1/file12.ext)
 * [file_in_root.ext](./file_in_root.ext)
 * [README.md](./README.md)
 * [dir3](./dir3)








